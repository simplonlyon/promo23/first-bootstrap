# first-bootstrap



## Premier exo bootstrap
1. Créer un fichier index.html et dedans charger le css de bootstrap (aller voir sur le getting started de la doc ⚠️ prenez bien la dernière version 5.2)
2. Pour voir que ça a bien marché, essayer de rajouter un button dans le HTML et mettez lui comme classe btn et btn-primary, ça devrait faire un joli bouton bleu
3. Créer un style css dans lequel on va dire que, par exemple, les articles font une height de 40px et ont une border
4. Dans le HTML rajouter des articles et en utilisant les classes row et col de bootstrap faire que l'affichage soit :
   * Sur mobile, un article par ligne
   * Sur tablette, 2 articles par ligne
   * Sur desktop, 4 articles par ligne
